import * as React from 'react';
import './App.css';

// tslint:disable

interface IShowProps {
  text: string,
}

const Show = (props: IShowProps) => {
  return <div>{props.text}</div>
}

interface IButtonWithInputState {
  inputText?: string;
  timesClicked: number,
}

class ButtonWithInput extends React.Component<any, IButtonWithInputState> {
  constructor(props: any) {
    super(props);
    this.state = {
      timesClicked: 0,
    }
  }

  handleClick = (timesClicked: any) => {
    this.setState({
      timesClicked,
    })
  }

  handleOnChange = (e: any) => {
    this.setState({
      inputText: e.target.value,
    })
  }

  public render() {
    return (
      <div>
        <input type="text" onChange={this.handleOnChange} />
        <Button onClick={this.handleClick}>Click me</Button>
        <label>{this.state.inputText}</label>
        <label>Times clicked: {this.state.timesClicked}</label>
      </div>
    )
  }
}

class Button extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      timesClicked: 0,
    }
  }

  handleClick = (e: any) => {
    let timesClicked = this.state.timesClicked;
    this.setState({
      timesClicked: timesClicked + 1,
    })
    this.props.onClick(timesClicked);
  }

  public render() {
    return (
      <button onClick={this.handleClick}>Click me</button>
    );
  }
}

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <Show text="Hello" />
        <ButtonWithInput />
      </div>
    );
  }
}

export default App;
